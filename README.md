# Tiny Todo
Tiny Todo is an experiment in minification, with the goal of making the smallest fully featured todo list program.

Usage is as follows:

```
$ todo
  1. A
  2. B

$ todo add C
Added todo "C".
  1. A
  2. B
  3. C

$ todo move 3 1
Moved todo "C" to position 1.
  1. C
  2. A
  3. B

$ todo remove 2
Removed todo "A".
  1. C
  2. B

$ todo clear
Cleared todos.
```

I chose to use Perl for this project since it allows for extremely short code and because of how humorously unreadable it can become.

I started with a simple script (todo original.pl: 1714 characters) that allows for all of the functions shown above with user feedback and input checking. Todos are stored in a file called ".t" in the home directory.

```perl
#!/usr/bin/env perl

'use strict';

$file = $ENV{HOME} . "/.t";
@list = ();

if(open(DATA, '<' . $file)) {
  while(<DATA>) {
    push(@list, $_);
  }
  close(DATA);
}
chomp(@list);

if(@ARGV) {
  if($ARGV[0] eq "help") {
    print "Example usage:\n  \$ todo\n    1. A\n    2. B\n\n  \$ todo add C\n  Added todo \"C\".\n    1. A\n    2. B\n    3. C\n\n  \$ todo move 3 1\n  Moved todo \"C\" to position 1.\n    1. C\n    2. A\n    3. B\n\n  \$ todo remove 2\n  Removed todo \"A\".\n    1. C\n    2. B\n\n  \$ todo clear\n  Cleared todos.\n\n";
    exit;
  }
  elsif($ARGV[0] eq "add") {
    $item = join(' ', @ARGV[1..$#ARGV]);
    push(@list, $item);
    print "Added todo \"$item\".\n";
  }
  elsif($ARGV[0] eq "clear") {
    @list = ();
    print "Cleared todos.\n";
  }
  elsif($ARGV[0] eq "move") {
    $index1 = $ARGV[1] || 1;
    $index2 = $ARGV[2] || 1;
    if($index1 > 0 && $index1 <= $#list + 1 && $index2 > 0 && $index2 <= $#list + 1) {
      $todo = $list[$index1 - 1];
      splice(@list, $index1 - 1, 1);
      splice(@list, $index2 - 1, 0, $todo);
      print "Moved todo \"$todo\" to position $index2.\n";
    }
    else {
      print "Could not move todo from $index1 to $index2.\n";
    }
  }
  elsif($ARGV[0] eq "remove") {
    $index = $ARGV[1] || 1;
    if($index > 0 && $index <= $#list + 1) {
      $todo = $list[$index - 1];
      splice(@list, $index - 1, 1);
      print "Removed todo \"$todo\".\n";
    }
    else {
      print "Could not remove todo $index.\n";
    }
  }
  else {
    print "Unknown argument \"$ARGV[0]\".\n"
  }
}

for $i (1..($#list + 1)) {
  print "  $i. $list[$i - 1]\n";
}
print "\n";

open(DATA, '>' . $file);
foreach(@list) {
  print DATA $_."\n";
}
close(DATA);
```

After removing whitespace and some punctuation, changing variable names, and reordering items, I was able to achieve the following minification (todo.pl: 950 characters) without losing any functionality.

```perl
#!/usr/bin/env perl
sub p{print@_}@a=@ARGV;$b=$a[0];$f=$ENV{HOME}."/.todo";(open D,'<'.$f)?chomp(@l=<D>):0;@a?($x=$a[1]||1,$b eq"help"?die"Example usage:$/  \$ todo$/    1. A$/    2. B$/$/  \$ todo add C$/  Added todo \"C\".$/    1. A$/    2. B$/    3. C$/$/  \$ todo move 3 1$/  Moved todo \"C\" to position 1.$/    1. C$/    2. A$/    3. B$/$/  \$ todo remove 2$/  Removed todo \"A\".$/    1. C$/    2. B$/$/  \$ todo clear$/  Cleared todos.$/$/":$b eq"add"?(push(@l,$t=join$",@a[1..$#a]),p"Added todo \"$t\".$/"):$b eq"clear"?(@l=(),p"Cleared todos.$/"):$b eq"move"?($y=$a[2]||1,$x>0&&$x<$#l+2&&$y>0&&$y<$#l+2?($t=splice(@l,$x-1,1),p"Moved todo \"$t\" to position $y.$/",splice@l,$y-1,0,$t):p"Could not move todo from $x to $y.$/"):$b eq"remove"?($x>0&&$x<$#l+2?(p"Removed todo \"".splice(@l,$x-1,1)."\".$/"):p"Could not remove todo $x.$/"):p"Unknown argument \"$b\".$/"):0;for$i(1..$#l+1){p"  $i. $l[$i-1]$/"}p$/;open D,'>'.$f;print D $_.$/foreach@l
```

But let's go deeper

By sacrificing some functionality, the program can be made even shorter. Simply removing the help screen significantly shortens the program (tod.pl: 624 characters).

```perl
#!/usr/bin/env perl
sub p{print@_}@a=@ARGV;$b=$a[0];$f=$ENV{HOME}."/.t";(open D,'<'.$f)?chomp(@l=<D>):0;@a?($x=$a[1]||1,$b eq"add"?(push(@l,$t=join$",@a[1..$#a]),p"Added todo \"$t\".$/"):$b eq"clear"?(@l=(),p"Cleared todos.$/"):$b eq"move"?($y=$a[2]||1,$x>0&&$x<$#l+2&&$y>0&&$y<$#l+2?($t=splice(@l,$x-1,1),p"Moved todo \"$t\" to position $y.$/",splice@l,$y-1,0,$t):p"Could not move todo from $x to $y.$/"):$b eq"remove"?($x>0&&$x<$#l+2?(p"Removed todo \"".splice(@l,$x-1,1)."\".$/"):p"Could not remove todo $x.$/"):p"Unknown argument \"$b\".$/"):0;for$i(1..$#l+1){p"  $i. $l[$i-1]$/"}p$/;open D,'>'.$f;print D $_.$/foreach@l
```

Removing user messages also has a significant effect (to.pl: 408 characters).

```perl
#!/usr/bin/env perl
sub p{print@_}@a=@ARGV;$b=$a[0];$f=$ENV{HOME}."/.t";(open D,'<'.$f)?chomp(@l=<D>):0;@a?($x=$a[1]||1,$b eq"add"?push(@l,join$",@a[1..$#a]):$b eq"clear"?@l=():$b eq"move"?($y=$a[2]||1,$x>0&&$x<$#l+2&&$y>0&&$y<$#l+2?($t=splice(@l,$x-1,1),splice@l,$y-1,0,$t):0):$b eq"remove"?($x>0&&$x<$#l+2?splice@l,$x-1,1:0):0):0;for$i(1..$#l+1){p"  $i. $l[$i-1]$/"}p$/;open D,'>'.$f;print D $_.$/foreach@l
```

After removing error checking (which can produce some unexpected results on invalid input) the program is about as small as it can get (t.pl: 342 characters).

```perl
#!/usr/bin/env perl
sub p{print@_}@a=@ARGV;$b=$a[0];$f=$ENV{HOME}."/.t";(open D,'<'.$f)?chomp(@l=<D>):0;@a?($x=$a[1]||1,$b eq"add"?push(@l,join$",@a[1..$#a]):$b eq"clear"?@l=():$b eq"move"?($t=splice(@l,$x-1,1),splice@l,$y-1,0,$t):$b eq"remove"?splice@l,$x-1,1:0):0;for$i(1..$#l+1){p"  $i. $l[$i-1]$/"}p$/;open D,'>'.$f;print D $_.$/foreach@l
```

However it is possible to save even more space by removing the formatted list output (with numbers and indentation) which results in this (t: 315 characters).

```perl
#!/usr/bin/env perl
sub p{print@_}@a=@ARGV;$b=$a[0];$f=$ENV{HOME}."/.t";(open D,'<'.$f)?chomp(@l=<D>):0;@a?($x=$a[1]||1,$b eq"add"?push(@l,join$",@a[1..$#a]):$b eq"clear"?@l=():$b eq"move"?($t=splice(@l,$x-1,1),splice@l,$y-1,0,$t):$b eq"remove"?splice@l,$x-1,1:0):0;$,=$/;p@l,'';open D,'>'.$f;print D $_.$/foreach@l
```

The script can also be converted to a one-liner that can run directly in the shell (304 characters).

```bash
perl -e'sub p{print@_}@a=@ARGV;$b=$a[0];$f=$ENV{HOME}."/.t";(open D,"<".$f)?chomp(@l=<D>):0;@a?($x=$a[1]||1,$b eq"add"?push(@l,join$",@a[1..$#a]):$b eq"clear"?@l=():$b eq"move"?($t=splice(@l,$x-1,1),splice@l,$y-1,0,$t):$b eq"remove"?splice@l,$x-1,1:0):0;$,=$/;p@l,"";open D,">".$f;print D $_.$/foreach@l'
```