#!/usr/bin/env perl

'use strict';

$file = $ENV{HOME} . "/.t";
@list = ();

if(open(DATA, '<' . $file)) {
  while(<DATA>) {
    push(@list, $_);
  }
  close(DATA);
}
chomp(@list);

if(@ARGV) {
  if($ARGV[0] eq "help") {
    print "Example usage:\n  \$ todo\n    1. A\n    2. B\n\n  \$ todo add C\n  Added todo \"C\".\n    1. A\n    2. B\n    3. C\n\n  \$ todo move 3 1\n  Moved todo \"C\" to position 1.\n    1. C\n    2. A\n    3. B\n\n  \$ todo remove 2\n  Removed todo \"A\".\n    1. C\n    2. B\n\n  \$ todo clear\n  Cleared todos.\n\n";
    exit;
  }
  elsif($ARGV[0] eq "add") {
    $item = join(' ', @ARGV[1..$#ARGV]);
    push(@list, $item);
    print "Added todo \"$item\".\n";
  }
  elsif($ARGV[0] eq "clear") {
    @list = ();
    print "Cleared todos.\n";
  }
  elsif($ARGV[0] eq "move") {
    $index1 = $ARGV[1] || 1;
    $index2 = $ARGV[2] || 1;
    if($index1 > 0 && $index1 <= $#list + 1 && $index2 > 0 && $index2 <= $#list + 1) {
      $todo = $list[$index1 - 1];
      splice(@list, $index1 - 1, 1);
      splice(@list, $index2 - 1, 0, $todo);
      print "Moved todo \"$todo\" to position $index2.\n";
    }
    else {
      print "Could not move todo from $index1 to $index2.\n";
    }
  }
  elsif($ARGV[0] eq "remove") {
    $index = $ARGV[1] || 1;
    if($index > 0 && $index <= $#list + 1) {
      $todo = $list[$index - 1];
      splice(@list, $index - 1, 1);
      print "Removed todo \"$todo\".\n";
    }
    else {
      print "Could not remove todo $index.\n";
    }
  }
  else {
    print "Unknown argument \"$ARGV[0]\".\n"
  }
}

for $i (1..($#list + 1)) {
  print "  $i. $list[$i - 1]\n";
}
print "\n";

open(DATA, '>' . $file);
foreach(@list) {
  print DATA $_."\n";
}
close(DATA);